import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

   public static void main (String[] param) {

      /*
      It is not necessary to initialize primitive (int, double...) or objects of classes (String...)
      before. We can convert the values directly.
      I just wanted to show them.
       */
    
      // conversion double -> String
      /*
      I haven't found "given" double number.
      So let it be 37.2
       */
      double d = 37.2;
      String doubleToString = Double.toString(d);
      System.out.println(doubleToString);

      // conversion String -> int
      //we will get NumberFormatException during conversion in case of illegal string
      String str = "1";
      int number = 0;
      try {
         number = Integer.parseInt(str);
      } catch (NumberFormatException e) {
         System.out.println("String can not be converted to int");
      }
      System.out.println(number);

      // "hh:mm:ss"
      String currentTime = new SimpleDateFormat("HH:mm:ss").format(System.currentTimeMillis());
      System.out.println(currentTime);
      

      // cos 45 deg
      double cos = Math.cos(Math.toRadians(45.0));
      System.out.println(cos);

      // table of square roots
      for (int i = 0; i < 101; i += 5) {
         System.out.printf("argument: %d, square root: %f\n", i, Math.sqrt(i));
      }

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string
      //I used already given firstString in this task
      String reversedString = new StringBuilder(firstString).reverse().toString();
      System.out.printf("\"%s\" -> \"%s\"\n", firstString, reversedString);

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + nw);

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
      long currTime = System.currentTimeMillis();
      try {
         Thread.sleep(3000);
      } catch (Exception ignored) {}
      long delay = System.currentTimeMillis() - currTime;
      System.out.println(delay);

      final int LSIZE = 100;
      /*
      int is primitive, Integer is class, which wraps int into an Object and gives us more
      flexibility in work with int data.
      However, such flexibility costs us a memory (Integer takes 4 time more memory than int, int takes 32 bits)
       */
      ArrayList<Integer> randList = new ArrayList<>(LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (generaator.nextInt(1000));
      }

      // minimal element
      /*
      In our case, the easiest way is to use Collections class,
      as we have List of Comparables. It consists of methods that operate on
      collections of COMPARABLE objects.
       */
      int min = Collections.min(randList);
      System.out.println(min);

      // HashMap tasks:
      //    create
      HashMap<String, String> mySubjects = new HashMap<>();
      mySubjects.put("ICA0013", "Fundamentals of networking");
      mySubjects.put("ICD0001", "Algorithms and Data Structures");
      mySubjects.put("ICD0006", "JavaScript");
      mySubjects.put("ICD0009", "Building Distributed Systems");
      mySubjects.put("ICD0015", "ASP.NET Web applications");
      //    print all keys
      for (String key: mySubjects.keySet()) {
         System.out.println(key);
      }
      //    remove a key
      mySubjects.remove("ICA0013");
      //    print all pairs
      for (String key: mySubjects.keySet()) {
         System.out.printf("%s -> %s\n", key, mySubjects.get(key));
      }

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      int counter = 0;
      String[] array = text.split("\\s+");
      for (String s: array) {
         if (!s.equals("")) {
            counter ++;
         }
      }

      return counter;
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {

      StringBuilder answer = new StringBuilder();
       char[] stringArray   = s.toCharArray();
      for (char c : stringArray) {
         if (Character.isUpperCase(c)) {
              answer.append(Character.toLowerCase(c));

         }
         else {
            answer.append(Character.toUpperCase(c));
         }
      }
      return answer.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T> void reverseList (List<T> list)
      throws UnsupportedOperationException {
      Collections.reverse(list);
   }
}
